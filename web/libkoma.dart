part of libshogi;

/**
 * マスの場所を保持するクラス
 */
class MasuPos {
  int x_;
  int y_;
  int get suji => x_;
  void set suji(int x) {
    x = x_;
  }
  int get dan => y_;
  void set dan(int y) {
    y_ = y;
  }
  MasuPos(this.x_, this.y_);
}

class MasuPosList {
  List<MasuPos> masupos_;
  /*void add(int x, int y) {
    masupos_.add(new MasuPos(x, y));
  }*/
  void add(MasuPos pos) {
    masupos_.add(pos);
  }
  int size() => masupos_.length;
  MasuPos at(int n) => masupos_[n];
  MasuPos operator[](int n) => masupos_[n];
  MasuPosList() {
    masupos_ = new List<MasuPos>(); 
  }
}

/**
 * ある駒の利きを保持するクラス 
 */
class Kiki {
  MasuPosList rin8_;  // 隣接８マスの利き
  MasuPosList straight_;  // 隣接８マス以外の利き

  int rin8Size() => rin8_.size();
  int straightSize() => straight_.size();

  MasuPos rin8At(int n) => rin8_[n];
  MasuPos straightAt(int n) => straight_[n];

  void rin8Add(MasuPos pos) {
    rin8_.add(pos);
  }
  void straightAdd(MasuPos pos) {
    straight_.add(pos);
  }
}

/**
 * 駒が動ける１方向を表す。 
 */
class Movement {
  int dx;
  int dy;
  bool straight;

  //Movement()
  //    :x = 0, y = 0, straight = false;

  Movement(this.dx, this.dy, this.straight);
}

/**
 * 駒の情報を保持するクラス
 */
class Koma {
  static int SENTEBAN = 1;
  static int GOTEBAN = 2;
  static int AKI = 3;

  static int NARAZU = 1;
  static int NARI = 2;

  static int NARENAI = 1;
  static int NARU = 2;
  static int NARERU = 3;
  static int NATTA = 4;

  static int NoID = -1;
  static int FuID = 0;
  static int KyoshaID = 1;
  static int KeimaID = 2;
  static int GinID = 3;
  static int KinID = 4;
  static int KakuID = 5;
  static int HishaID = 6;
  static int GyokuID = 7;

  //-- CSA
  static String FuStr = 'FU';
  static String KyoshaStr = 'KY';
  static String KeimaStr = 'KE';
  static String GinStr = 'GI';
  static String KinStr = 'KI';
  static String KakuStr = 'KA';
  static String HishaStr = 'HI';
  static String GyokuStr = 'OU';
  static String NFuStr = 'TO';
  static String NKyoshaStr = 'NY';
  static String NKeimaStr = 'NE';
  static String NGinStr = 'NG';
  //static String NKinStr = 'KI';
  static String NKakuStr = 'UM';
  static String NHishaStr = 'RY';
  //static String NGyokuStr = 'OU';

  //-- KIF
  static String FuStrKIF = '歩';
  static String KyoshaStrKIF = '香';
  static String KeimaStrKIF = '桂';
  static String GinStrKIF = '銀';
  static String KinStrKIF = '金';
  static String KakuStrKIF = '角';
  static String HishaStrKIF = '飛';
  static String GyokuStrKIF = '玉';
  static String NFuStrKIF = 'と';
  static String NKyoshaStrKIF = '成香';
  static String NKeimaStrKIF = '成桂';
  static String NGinStrKIF = '成銀';
  //static String NKinStrKIF = '成金';
  static String NKakuStrKIF = '馬';
  static String NHishaStrKIF = '竜';
  //static String NGyokuStrKIF = '王';
  static String NariStrKIF = '成';
  static String UchiStrKIF = '打';
  static String DouStrKIF = '同　';
  static String FunariStr = '不成';

  static List KomaStrTbl = [
   '歩', '香', '桂', '銀', '金', '角', '飛', '玉',
   'と', '成香', '成桂', '成銀', '成金', '馬', '竜', '王'];

  static String SenteStr = '▲';
  static String GoteStr = '△';
  static String AkiStr = ' ';
  static String SenteStrKIF = ' ';
  static String GoteStrKIF = 'v';
  static String AkiStrKIF = ' ・';
  static String SenteStrCSA = '+';
  static String GoteStrCSA = '-';
  static String AkiStrCSA = ' * ';
  static String SenteStrOrg = '先手';
  static String GoteStrOrg = '後手';

  static String ToryoStr = '投了';
  static String ToryoStrCSA = '%TORYO';
  static String TsumiStrCSA = '%TSUMI';

  static List<Movement> FuMovement = [new Movement(0, 1, false)];
  static List<Movement> KyoshaMovement = [new Movement(0, 1, true)];
  static List<Movement> KeimaMovement = [new Movement(1, 2, false), new Movement(-1, 2, false)];
  static List<Movement> GinMovement = [new Movement(1, 1, false), new Movement(0, 1, false), new Movement(-1, 1, false),
                            new Movement(1, -1, false), new Movement(-1, -1, false)];
  static List<Movement> KinMovement = [new Movement(1, 1, false), new Movement(0, 1, false), new Movement(-1, 1, false),
                            new Movement(1, 0, false), new Movement(-1, 0, false), new Movement(0, -1, false)];
  static List<Movement> KakuMovement = [new Movement(1, 1, true), new Movement(-1, -1, true), new Movement(-1, 1, true),
                             new Movement(1, -1, true)];
  static List<Movement> HishaMovement = [new Movement(1, 0, true), new Movement(-1, 0, true), new Movement(0, 1, true),
                              new Movement(0, -1, true)];
  static List<Movement> UmaMovement = [new Movement(1, 1, true), new Movement(-1, -1, true), new Movement(-1, 1, true),
                            new Movement(1, -1, true), new Movement(0, 1, false), new Movement(1, 0, false),
                            new Movement(-1, 0, false), new Movement(0, -1, false)];
  static List<Movement> RyuMovement = [new Movement(1, 0, true), new Movement(-1, 0, true), new Movement(0, 1, true),
                            new Movement(0, -1, true), new Movement(1, 1, false), new Movement(1, -1, false),
                            new Movement(-1, 1, false), new Movement(-1, -1, false)];
  static List<Movement> GyokuMovement = [new Movement(1, 1, false), new Movement(0, 1, false), new Movement(-1, 1, false),
                              new Movement(1, 0, false), new Movement(-1, 0, false), new Movement(1, -1, false),
                              new Movement(0, -1, false), new Movement(-1, -1, false)];

  static List<String> ZenkakuNum =
   ['１', '２', '３', '４', '５', '６', '７', '８', '９'];
  static List<String> KanjiNum =
   ['一', '二', '三', '四', '五', '六', '七', '八', '九'];

  // members
  int teban = Koma.AKI;
  String strtype = '* ';
  String strntype = '* ';
  String strtypeKIF = '* ';
  String strntypeKIF = '* ';
  String strtypeCSA = '* ';
  String strntypeCSA = '* ';
  int nari = NARAZU;
  int id = NoID;
  int x = -1;
  int y = -1;
  int uchi_limit = 0;  // 駒を打つときに空けないといけない段数

  void setTeban(int t) {
    teban = t;
  }
  void setx(int n) {x = n;}
  void sety(int n) {y = n;}
  void setxy(int xx, int yy) {
    x = xx;
    y = yy;
  }

  /**
   * 表示用の文字列の取得
   *
   * @return {String} 表示用の文字列
   */
  String getStr() {
    String str;

    if (teban == SENTEBAN) {
      str = SenteStr;
    } else if (teban == GOTEBAN) {
      str = GoteStr;
    } else {
      str = AkiStr;
      return str;
    }
    if (nari == NARI) {
      str += strntype;
    } else {
      str += strtype;
    }
    return str;
  }

  /**
   * CSA表示用の文字列の取得
   *
   * @return {String} 表示用の文字列
   */
  String getShortStrCSA() {
    String str;
    if (teban == SENTEBAN) {
      str = SenteStrCSA;
    } else if (teban == GOTEBAN) {
      str = GoteStrCSA;
    } else {
      str = AkiStrCSA;
      return str;
    }
    if (nari == NARI) {
      str += strntypeCSA;
    } else {
      str += strtypeCSA;
    }
    return str;
  }

  /**
   * CSA表示用の文字列の取得
   *
   * @return {String} 表示用の文字列
   */
  String getShortStrKIF() {
    String str;
    if (teban == SENTEBAN) {
      str =SenteStrKIF;
    } else if (teban == GOTEBAN) {
      str = GoteStrKIF;
    } else {
      str = AkiStrKIF;
      return str;
    }
    if (nari == NARI) {
      str += strntypeKIF;
    } else {
      str += strtypeKIF;
    }
    return str;
  }

  /**
   * 動ける方向のリストを返す。
   *
   * @return {MasuPosList} 空のArray
   */
  List<Movement> movement() {
    return [];
  }

  /**
   * その他の駒がないとしてこれ以上動けるか
   *
   * @param {int} oy 現在地
   *
   * @return {Boolean} true:まだ動ける, false:もう無理。
   */
  bool checkMovable(int oy) {
    return true;
  }

  /**
   * 利いているマスのリストを返す。他のコマの影響を考慮。
   *
   * @param {int} ox 現在地
   * @param {int} oy 現在地
   *
   * @return {Kiki} 利いているマスのリスト
   */
  Kiki getKiki(int ox, int oy) {
    return new Kiki();
  }
  /**
   * 利いているマスのリストを返す。他のコマの影響を無視。
   *
   * @param {int} ox 現在地
   * @param {int} oy 現在地
   *
   * @return {Kiki} 利いているマスのリスト
   */
  Kiki getKiki2(int ox, int oy) {
    return new Kiki();
  }

  /**
   * 成れるかどうかをチェック
   *
   * @param {int} fromy 移動元の座標
   * @param {int} toy   移動先の座標
   *
   * @return {int} Koma.NARENAI 成れない
   *               Koma.NARERU  成れる
   *               Koma.NARU    成らないといけない
   *               Koma.NATTA   成った後
   */
  int checkNari(int fromy, int toy) {
    if (this.nari == Koma.NARI) {
      return Koma.NATTA;
    }
    if (this.teban == Koma.SENTEBAN) {
      // 動けるかのチェック
      var ugokeru = this.checkMovable(toy);
      if (ugokeru) {
        // 動ければNARERU
        if (fromy < 3 || toy < 3) {
          return Koma.NARERU;
        }
      } else {
        // 動けなければNARU
        if (fromy < 3 || toy < 3) {
          return Koma.NARU;
        } else {
          return Koma.NARENAI;
        }
      }
      //return this.nareru;
    }
    if (this.teban == Koma.GOTEBAN) {
      // 動けるかのチェック
      var ugokeru = this.checkMovable(toy);
      if (ugokeru) {
        // 動ければNARERU
        if (fromy >= 6 || toy >= 6) {
          return Koma.NARERU;
        }
      } else {
        // 動けなければNARU
        if (fromy >= 6 || toy >= 6) {
          return Koma.NARU;
        } else {
          return Koma.NARENAI;
        }
      }
    }
    return Koma.NARENAI;
  }

  Koma()
      :teban = AKI, x = -1, y = -1 {
    ;
  }
  //Koma(this.teban, this.x, this.y);

  /**
   * CSA形式で１手を出力
   *
   * @param {int} fromx 移動元の座標
   * @param {int} fromy 移動元の座標
   * @param {int} tox 移動先の座標
   * @param {int} toy 移動先の座標
   *
   *
   * @return {String} １手分の棋譜
   */
  String kifuCSA(int fromx, int fromy, int tox, int toy) {
    fromx++;
    fromy++;
    tox++;
    toy++;

    String str;
    if (teban == Koma.SENTEBAN) {
      str = Koma.SenteStrCSA;
    } else if (teban == Koma.GOTEBAN) {
      str = Koma.GoteStrCSA;
    }
    str += fromx.toString();
    str += fromy.toString();
    str += tox.toString();
    str += toy.toString();
    if (nari == Koma.NARI) {
      str += strntypeCSA;
    } else {
      str += strtypeCSA;
    }
    return str;
  }

  /**
   * KIF形式で１手を出力
   *
   * @param {int} fromx 移動元の座標
   * @param {int} fromy 移動元の座標
   * @param {int} tox 移動先の座標
   * @param {int} toy 移動先の座標
   * @param {int} lastx 直前の手の移動先の座標
   * @param {int} lasty 直前の手の移動先の座標
   * @param {int} nari 成ったかどうか
   *
   * @return {String} １手分の棋譜
   */
  kifuKIF(int fromx, int fromy, int tox, int toy, int lastx, int lasty,
           int  nari) {
    fromx++;
    fromy++;

    String str = '';
    /*if (this.teban == Koma.SENTEBAN) {
     str = SenteStrKIF;
    } else if (this.teban == Koma.GOTEBAN) {
     str = GoteStrKIF;
    }*/
    if (tox == lastx && toy == lasty) {
     str += DouStrKIF;
    } else {
     str += ZenkakuNum[tox];
     str += KanjiNum[toy];
    }
    if (this.nari == NARI) {
     if (nari == NARI) {
      str += strtypeKIF;
      str += NariStrKIF;
     } else {
      str += strntypeKIF;
     }
    } else {
     str += strtypeKIF;
     if (fromx == 0) {
      str += UchiStrKIF;
     }
    }
    if (fromx != 0) {
     str += '(' + fromx.toString() + fromy.toString() + ')';
    }
    return str;
  }

  /**
   * 独自形式で１手を出力
   *
   * @param {int} fromx 移動元の座標
   * @param {int} fromy 移動元の座標
   * @param {int} tox   移動先の座標
   * @param {int} toy   移動先の座標
   * @param {int} lastx 直前の手の移動先の座標
   * @param {int} lasty 直前の手の移動先の座標
   * @param {int} nata  成ったかどうか
   *
   * @return {String} １手分の棋譜
   */
  String kifuKIFU(int fromx, int fromy, int tox, int toy, int lastx, int lasty, int nata) {
    fromx++;
    fromy++;

    String str;
    if (teban == SENTEBAN) {
      str = SenteStrOrg;
    } else if (this.teban == GOTEBAN) {
      str = GoteStrOrg;
    }
    if (tox == lastx && toy == lasty) {
      str += DouStrKIF;
    } else {
      str += ZenkakuNum[tox];
      str += KanjiNum[toy];
    }
    if (nari == NARI) {
      if (nata == NARI) {
        str += strtypeKIF;
        str += NariStrKIF;
      } else {
        str += strntypeKIF;
      }
    } else if (nata == NARERU) {
      str += strtypeKIF;
      str += FunariStr;
    } else {
      str += strtypeKIF;
      if (fromx == 0) {
        str += UchiStrKIF;
      }
    }
    if (fromx != 0) {
      str += ' (' + fromx.toString() + fromy.toString() + ')';
    }
    return str;
  }

  /**
   * CSA形式で１手を短め出力
   *
   * @param {int} x 座標
   * @param {int} y 座標
   *
   * @return {String} １手分の棋譜
   * 
   * @note "+19HI"とか"-55KA"とか
   */
  String kifuShortCSA(int tx, int ty) {
    tx++;
    ty++;

    String str;
    if (this.teban == Koma.SENTEBAN) {
      str = Koma.SenteStrCSA;
    } else if (this.teban == Koma.GOTEBAN) {
      str = Koma.GoteStrCSA;
    }
    str += tx.toString();
    str += ty.toString();
    if (this.nari == Koma.NARI) {
      str += this.strntypeCSA;
    } else {
      str += this.strtypeCSA;
    }
    return str;
  }
}

/**
 * 歩クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Fu extends Koma {
  Fu(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;
    this.strtype = Koma.FuStrKIF;
    this.strntype = Koma.NFuStrKIF;
    this.strtypeKIF = Koma.FuStrKIF;
    this.strntypeKIF = Koma.NFuStrKIF;
    this.strtypeCSA = Koma.FuStr;
    this.strntypeCSA = Koma.NFuStr;
    this.id = Koma.FuID;
    this.uchi_limit = 1;
  }

  List<Movement> movement() {
    if (this.nari == Koma.NARI) {
      return Koma.KinMovement;
    } else {
      return Koma.FuMovement;
    }
  }

  /**
   * その他の駒がないとしてこれ以上動けるか
   *
   * @param {int} oy 現在地
   *
   * @return {Boolean} true:まだ動ける, false:もう無理。
   */
  bool checkMovable(int oy) {
    if (teban == Koma.SENTEBAN) {
      if (oy == 0) {
        return false;
      }
    } else {
      if (oy == 8) {
        return false;
      }
    }
    return true;
  }

  /**
   * 打てるマスのリストを返す。(二歩対策)
   *
   * @override
   *
   * @return {Array} 打てるマスのリスト
   */
  MasuPosList getUchable() {
   int starty = 0;
   int endy = 9;
   if (teban == Koma.SENTEBAN) {
     starty = 1;
   } else {
     endy = 8;
   }
   MasuPosList list;
   for (int i = 0; i < 9; ++i) {
     if (check2FU(i, starty, endy)) {
       continue;
     }
     //for (var j = starty; j < endy; ++j) {
     //  if (ban[i][j].koma.teban == Koma.AKI) {
     //    list.add(MasuPos(i, j));
     //  }
     //}
   }
   return list;
  }

  /**
   * 二歩になるかどうかチェックする。
   *
   * @param {int} x      チェックする筋
   * @param {int} starty チェックする範囲
   * @param {int} endy   チェックする範囲
   *
   * @return {Boolean} true:二歩になる, false:ならない
   */
  bool check2FU(int x, int starty, int endy) {
    /*for (int j = starty; j < endy; ++j) {
     if (ban[x][j].koma.id == Koma.FuID &&
         ban[x][j].koma.nari == Koma.NARAZU &&
         ban[x][j].koma.teban == teban) {
      return true;
     }
    }*/
    return false;
  }
}

/**
 * 香車クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Kyosha extends Koma
{
  Kyosha(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;
    this.strtype = Koma.KyoshaStrKIF;
    this.strntype = Koma.NKyoshaStrKIF;
    this.strtypeKIF = Koma.KyoshaStrKIF;
    this.strntypeKIF = Koma.NKyoshaStrKIF;
    this.strtypeCSA = Koma.KyoshaStr;
    this.strntypeCSA = Koma.NKyoshaStr;
    this.id = Koma.KyoshaID;
    this.uchi_limit = 1;
  }

  /**
   * その他の駒がないとしてこれ以上動けるか
   *
   * @param {int} oy 現在地
   *
   * @return {Boolean} true:まだ動ける, false:もう無理。
   */
  bool checkMovable(int oy) {
    if (teban == Koma.SENTEBAN) {
      if (oy == 0) {
        return false;
      }
    } else {
      if (oy == 8) {
        return false;
      }
    }
    return true;
  }

  List<Movement> movement() {
    if (this.nari == Koma.NARI) {
      return Koma.KinMovement;
    } else {
      return Koma.KyoshaMovement;
    }
  }
}

/**
 * 桂馬クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Keima extends Koma {
  Keima(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;

    this.strtype = Koma.KeimaStrKIF;
    this.strntype = Koma.NKeimaStrKIF;
    this.strtypeKIF = Koma.KeimaStrKIF;
    this.strntypeKIF = Koma.NKeimaStrKIF;
    this.strtypeCSA = Koma.KeimaStr;
    this.strntypeCSA = Koma.NKeimaStr;
    this.id = Koma.KeimaID;
    this.uchi_limit = 2;
  }

  /**
   * その他の駒がないとしてこれ以上動けるか
   *
   * @param {int} oy 現在地
   *
   * @return {Boolean} true:まだ動ける, false:もう無理。
   */
  bool checkMovable(int oy) {
    if (teban == Koma.SENTEBAN) {
      if (oy == 1) {
        return false;
      }
    } else {
      if (oy == 7) {
        return false;
      }
    }
    return true;
  }

  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {Array} 動ける方向のリスト
   */
  List<Movement> movement() {
    if (this.nari == Koma.NARI) {
      return Koma.KinMovement;
    } else {
      return Koma.KeimaMovement;
    }
  }
}

/**
 * 銀将クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Gin extends Koma {
  Gin(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;

    this.strtype = Koma.GinStrKIF;
    this.strntype = Koma.NGinStrKIF;
    this.strtypeKIF = Koma.GinStrKIF;
    this.strntypeKIF = Koma.NGinStrKIF;
    this.strtypeCSA = Koma.GinStr;
    this.strntypeCSA = Koma.NGinStr;
    this.id = Koma.GinID;
  }
  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {Array} 動ける方向のリスト
   */
  List<Movement> movement() {
    if (nari == Koma.NARI) {
      return Koma.KinMovement;
    } else {
      return Koma.GinMovement;
    }
  }
}

/**
 * 金将クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Kin extends Koma {
  Kin(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;

    this.strtype = Koma.KinStrKIF;
    this.strntype = Koma.KinStrKIF;
    this.strtypeKIF = Koma.KinStrKIF;
    this.strntypeKIF = Koma.KinStrKIF;
    this.strtypeCSA = Koma.KinStr;
    this.strntypeCSA = Koma.KinStr;
    this.id = Koma.KinID;
    this.nari = Koma.NARI;
  }

  /**
   * 初期化
   *
   * @param {int} teban 手番
   */
  void reset(int teban) {
    this.teban = teban;
    this.nari = Koma.NARI;
    this.x = -1;
    this.y = -1;
  }

  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {Array} 動ける方向のリスト
   */
  List<Movement> movement() {
    return Koma.KinMovement;
  }
}

/**
 * 角行クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Kaku extends Koma {
  Kaku(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;

    this.strtype = Koma.KakuStrKIF;
    this.strntype = Koma.NKakuStrKIF;
    this.strtypeKIF = Koma.KakuStrKIF;
    this.strntypeKIF = Koma.NKakuStrKIF;
    this.strtypeCSA = Koma.KakuStr;
    this.strntypeCSA = Koma.NKakuStr;
    this.id = Koma.KakuID;
  }
  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {Array} 動ける方向のリスト
   */
  List<Movement> movement() {
    if (this.nari == Koma.NARI) {
      return Koma.UmaMovement;
    } else {
      return Koma.KakuMovement;
    }
  }
}

/**
 * 飛車クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Hisha extends Koma {
  Hisha(int teban, int x, int y) {
    this.teban = teban;
    this.x = x;
    this.y = y;

    this.strtype = Koma.HishaStrKIF;
    this.strntype = Koma.NHishaStrKIF;
    this.strtypeKIF = Koma.HishaStrKIF;
    this.strntypeKIF = Koma.NHishaStrKIF;
    this.strtypeCSA = Koma.HishaStr;
    this.strntypeCSA = Koma.NHishaStr;
    this.id = Koma.HishaID;
  }
  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {Array} 動ける方向のリスト
   */
  List<Movement> movement() {
    if (this.nari == Koma.NARI) {
      return Koma.RyuMovement;
    } else {
      return Koma.HishaMovement;
    }
  }
}

/**
 * 玉将クラス
 *
 * @class
 *
 * @param {int} teban 先手後手
 * @param {int} x 座標
 * @param {int} y 座標
 */
class Gyoku extends Koma {
  Gyoku(int teban, int x, int y) {
    this.x = x;
    this.y = y;
    this.teban = teban;
    this.strtype = Koma.GyokuStrKIF;
    this.strntype = Koma.GyokuStrKIF;
    this.strtypeKIF = Koma.GyokuStrKIF;
    this.strntypeKIF = Koma.GyokuStrKIF;
    this.strtypeCSA = Koma.GyokuStr;
    this.strntypeCSA = Koma.GyokuStr;
    this.id = Koma.GyokuID;
    this.nari = Koma.NARI;
  }
  /**
   * 動ける方向のリストを返す。
   *
   * @override
   *
   * @return {List<Movement>} 動ける方向のリスト
   */
  List<Movement> movement() {
    return Koma.GyokuMovement;
  }
}
