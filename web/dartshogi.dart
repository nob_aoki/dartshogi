import 'dart:html';

//import 'libkoma.dart';
import 'libshogi.dart';

class BanUI
{
  List<Element> masu_;
  ShogiGame sg_;
  Element hovermasui_;
  static String colorActive_ = 'green';
  static String colorBan_ = 'transparent';
  static String colorMovable_ = 'pink';
  static String colorHover_ = 'yellow';
  bool enabled_;

  BanUI(ShogiGame sg) {
    masu_ = new List(Ban.DAN*Ban.SUJI);
    sg_ = sg;

    enabled_ = false;
  }
  void init() {
    Element e;
    int i = 0;
    e = querySelector('#b11');
    masu_[i] = e
        ..onClick.listen(click11)
        ..onMouseOut.listen(hoverout11)
        ..onMouseOver.listen(hoverin11);
    ++i;
    e = querySelector('#b12');
    masu_[i] = e
        ..onClick.listen(click12)
        ..onMouseOut.listen(hoverout12)
        ..onMouseOver.listen(hoverin12);
    ++i;
    e = querySelector('#b13');
    masu_[i] = e
        ..onClick.listen(click13)
        ..onMouseOut.listen(hoverout13)
        ..onMouseOver.listen(hoverin13);
    ++i;
    e = querySelector('#b14');
    masu_[i] = e
        ..onClick.listen(click14)
        ..onMouseOut.listen(hoverout14)
        ..onMouseOver.listen(hoverin14);
    ++i;
    e = querySelector('#b15');
    masu_[i] = e
        ..onClick.listen(click15)
        ..onMouseOut.listen(hoverout15)
        ..onMouseOver.listen(hoverin15);
    ++i;
    e = querySelector('#b16');
    masu_[i] = e
        ..onClick.listen(click16)
        ..onMouseOut.listen(hoverout16)
        ..onMouseOver.listen(hoverin16);
    ++i;
    e = querySelector('#b17');
    masu_[i] = e
        ..onClick.listen(click17)
        ..onMouseOut.listen(hoverout17)
        ..onMouseOver.listen(hoverin17);
    ++i;
    e = querySelector('#b18');
    masu_[i] = e
        ..onClick.listen(click18)
        ..onMouseOut.listen(hoverout18)
        ..onMouseOver.listen(hoverin18);
    ++i;
    e = querySelector('#b19');
    masu_[i] = e
        ..onClick.listen(click19)
        ..onMouseOut.listen(hoverout19)
        ..onMouseOver.listen(hoverin19);
    ++i;
    e = querySelector('#b21');
    masu_[i] = e
        ..onClick.listen(click21)
        ..onMouseOut.listen(hoverout21)
        ..onMouseOver.listen(hoverin21);
    ++i;
    e = querySelector('#b22');
    masu_[i] = e
        ..onClick.listen(click22)
        ..onMouseOut.listen(hoverout22)
        ..onMouseOver.listen(hoverin22);
    ++i;
    e = querySelector('#b23');
    masu_[i] = e
        ..onClick.listen(click23)
        ..onMouseOut.listen(hoverout23)
        ..onMouseOver.listen(hoverin23);
    ++i;
    e = querySelector('#b24');
    masu_[i] = e
        ..onClick.listen(click24)
        ..onMouseOut.listen(hoverout24)
        ..onMouseOver.listen(hoverin24);
    ++i;
    e = querySelector('#b25');
    masu_[i] = e
        ..onClick.listen(click25)
        ..onMouseOut.listen(hoverout25)
        ..onMouseOver.listen(hoverin25);
    ++i;
    e = querySelector('#b26');
    masu_[i] = e
        ..onClick.listen(click26)
        ..onMouseOut.listen(hoverout26)
        ..onMouseOver.listen(hoverin26);
    ++i;
    e = querySelector('#b27');
    masu_[i] = e
        ..onClick.listen(click27)
        ..onMouseOut.listen(hoverout27)
        ..onMouseOver.listen(hoverin27);
    ++i;
    e = querySelector('#b28');
    masu_[i] = e
        ..onClick.listen(click28)
        ..onMouseOut.listen(hoverout28)
        ..onMouseOver.listen(hoverin28);
    ++i;
    e = querySelector('#b29');
    masu_[i] = e
        ..onClick.listen(click29)
        ..onMouseOut.listen(hoverout29)
        ..onMouseOver.listen(hoverin29);
    ++i;
    e = querySelector('#b31');
    masu_[i] = e
        ..onClick.listen(click31)
        ..onMouseOut.listen(hoverout31)
        ..onMouseOver.listen(hoverin31);
    ++i;
    e = querySelector('#b32');
    masu_[i] = e
        ..onClick.listen(click32)
        ..onMouseOut.listen(hoverout32)
        ..onMouseOver.listen(hoverin32);
    ++i;
    e = querySelector('#b33');
    masu_[i] = e
        ..onClick.listen(click33)
        ..onMouseOut.listen(hoverout33)
        ..onMouseOver.listen(hoverin33);
    ++i;
    e = querySelector('#b34');
    masu_[i] = e
        ..onClick.listen(click34)
        ..onMouseOut.listen(hoverout34)
        ..onMouseOver.listen(hoverin34);
    ++i;
    e = querySelector('#b35');
    masu_[i] = e
        ..onClick.listen(click35)
        ..onMouseOut.listen(hoverout35)
        ..onMouseOver.listen(hoverin35);
    ++i;
    e = querySelector('#b36');
    masu_[i] = e
        ..onClick.listen(click36)
        ..onMouseOut.listen(hoverout36)
        ..onMouseOver.listen(hoverin36);
    ++i;
    e = querySelector('#b37');
    masu_[i] = e
        ..onClick.listen(click37)
        ..onMouseOut.listen(hoverout37)
        ..onMouseOver.listen(hoverin37);
    ++i;
    e = querySelector('#b38');
    masu_[i] = e
        ..onClick.listen(click38)
        ..onMouseOut.listen(hoverout38)
        ..onMouseOver.listen(hoverin38);
    ++i;
    e = querySelector('#b39');
    masu_[i] = e
        ..onClick.listen(click39)
        ..onMouseOut.listen(hoverout39)
        ..onMouseOver.listen(hoverin39);
    ++i;
    e = querySelector('#b41');
    masu_[i] = e
        ..onClick.listen(click41)
        ..onMouseOut.listen(hoverout41)
        ..onMouseOver.listen(hoverin41);
    ++i;
    e = querySelector('#b42');
    masu_[i] = e
        ..onClick.listen(click42)
        ..onMouseOut.listen(hoverout42)
        ..onMouseOver.listen(hoverin42);
    ++i;
    e = querySelector('#b43');
    masu_[i] = e
        ..onClick.listen(click43)
        ..onMouseOut.listen(hoverout43)
        ..onMouseOver.listen(hoverin43);
    ++i;
    e = querySelector('#b44');
    masu_[i] = e
        ..onClick.listen(click44)
        ..onMouseOut.listen(hoverout44)
        ..onMouseOver.listen(hoverin44);
    ++i;
    e = querySelector('#b45');
    masu_[i] = e
        ..onClick.listen(click45)
        ..onMouseOut.listen(hoverout45)
        ..onMouseOver.listen(hoverin45);
    ++i;
    e = querySelector('#b46');
    masu_[i] = e
        ..onClick.listen(click46)
        ..onMouseOut.listen(hoverout46)
        ..onMouseOver.listen(hoverin46);
    ++i;
    e = querySelector('#b47');
    masu_[i] = e
        ..onClick.listen(click47)
        ..onMouseOut.listen(hoverout47)
        ..onMouseOver.listen(hoverin47);
    ++i;
    e = querySelector('#b48');
    masu_[i] = e
        ..onClick.listen(click48)
        ..onMouseOut.listen(hoverout48)
        ..onMouseOver.listen(hoverin48);
    ++i;
    e = querySelector('#b49');
    masu_[i] = e
        ..onClick.listen(click49)
        ..onMouseOut.listen(hoverout49)
        ..onMouseOver.listen(hoverin49);
    ++i;
    e = querySelector('#b51');
    masu_[i] = e
        ..onClick.listen(click51)
        ..onMouseOut.listen(hoverout51)
        ..onMouseOver.listen(hoverin51);
    ++i;
    e = querySelector('#b52');
    masu_[i] = e
        ..onClick.listen(click52)
        ..onMouseOut.listen(hoverout52)
        ..onMouseOver.listen(hoverin52);
    ++i;
    e = querySelector('#b53');
    masu_[i] = e
        ..onClick.listen(click53)
        ..onMouseOut.listen(hoverout53)
        ..onMouseOver.listen(hoverin53);
    ++i;
    e = querySelector('#b54');
    masu_[i] = e
        ..onClick.listen(click54)
        ..onMouseOut.listen(hoverout54)
        ..onMouseOver.listen(hoverin54);
    ++i;
    e = querySelector('#b55');
    masu_[i] = e
        ..onClick.listen(click55)
        ..onMouseOut.listen(hoverout55)
        ..onMouseOver.listen(hoverin55);
    ++i;
    e = querySelector('#b56');
    masu_[i] = e
        ..onClick.listen(click56)
        ..onMouseOut.listen(hoverout56)
        ..onMouseOver.listen(hoverin56);
    ++i;
    e = querySelector('#b57');
    masu_[i] = e
        ..onClick.listen(click57)
        ..onMouseOut.listen(hoverout57)
        ..onMouseOver.listen(hoverin57);
    ++i;
    e = querySelector('#b58');
    masu_[i] = e
        ..onClick.listen(click58)
        ..onMouseOut.listen(hoverout58)
        ..onMouseOver.listen(hoverin58);
    ++i;
    e = querySelector('#b59');
    masu_[i] = e
        ..onClick.listen(click59)
        ..onMouseOut.listen(hoverout59)
        ..onMouseOver.listen(hoverin59);
    ++i;
    e = querySelector('#b61');
    masu_[i] = e
        ..onClick.listen(click61)
        ..onMouseOut.listen(hoverout61)
        ..onMouseOver.listen(hoverin61);
    ++i;
    e = querySelector('#b62');
    masu_[i] = e
        ..onClick.listen(click62)
        ..onMouseOut.listen(hoverout62)
        ..onMouseOver.listen(hoverin62);
    ++i;
    e = querySelector('#b63');
    masu_[i] = e
        ..onClick.listen(click63)
        ..onMouseOut.listen(hoverout63)
        ..onMouseOver.listen(hoverin63);
    ++i;
    e = querySelector('#b64');
    masu_[i] = e
        ..onClick.listen(click64)
        ..onMouseOut.listen(hoverout64)
        ..onMouseOver.listen(hoverin64);
    ++i;
    e = querySelector('#b65');
    masu_[i] = e
        ..onClick.listen(click65)
        ..onMouseOut.listen(hoverout65)
        ..onMouseOver.listen(hoverin65);
    ++i;
    e = querySelector('#b66');
    masu_[i] = e
        ..onClick.listen(click66)
        ..onMouseOut.listen(hoverout66)
        ..onMouseOver.listen(hoverin66);
    ++i;
    e = querySelector('#b67');
    masu_[i] = e
        ..onClick.listen(click67)
        ..onMouseOut.listen(hoverout67)
        ..onMouseOver.listen(hoverin67);
    ++i;
    e = querySelector('#b68');
    masu_[i] = e
        ..onClick.listen(click68)
        ..onMouseOut.listen(hoverout68)
        ..onMouseOver.listen(hoverin68);
    ++i;
    e = querySelector('#b69');
    masu_[i] = e
        ..onClick.listen(click69)
        ..onMouseOut.listen(hoverout69)
        ..onMouseOver.listen(hoverin69);
    ++i;
    e = querySelector('#b71');
    masu_[i] = e
        ..onClick.listen(click71)
        ..onMouseOut.listen(hoverout71)
        ..onMouseOver.listen(hoverin71);
    ++i;
    e = querySelector('#b72');
    masu_[i] = e
        ..onClick.listen(click72)
        ..onMouseOut.listen(hoverout72)
        ..onMouseOver.listen(hoverin72);
    ++i;
    e = querySelector('#b73');
    masu_[i] = e
        ..onClick.listen(click73)
        ..onMouseOut.listen(hoverout73)
        ..onMouseOver.listen(hoverin73);
    ++i;
    e = querySelector('#b74');
    masu_[i] = e
        ..onClick.listen(click74)
        ..onMouseOut.listen(hoverout74)
        ..onMouseOver.listen(hoverin74);
    ++i;
    e = querySelector('#b75');
    masu_[i] = e
        ..onClick.listen(click75)
        ..onMouseOut.listen(hoverout75)
        ..onMouseOver.listen(hoverin75);
    ++i;
    e = querySelector('#b76');
    masu_[i] = e
        ..onClick.listen(click76)
        ..onMouseOut.listen(hoverout76)
        ..onMouseOver.listen(hoverin76);
    ++i;
    e = querySelector('#b77');
    masu_[i] = e
        ..onClick.listen(click77)
        ..onMouseOut.listen(hoverout77)
        ..onMouseOver.listen(hoverin77);
    ++i;
    e = querySelector('#b78');
    masu_[i] = e
        ..onClick.listen(click78)
        ..onMouseOut.listen(hoverout78)
        ..onMouseOver.listen(hoverin78);
    ++i;
    e = querySelector('#b79');
    masu_[i] = e
        ..onClick.listen(click79)
        ..onMouseOut.listen(hoverout79)
        ..onMouseOver.listen(hoverin79);
    ++i;
    e = querySelector('#b81');
    masu_[i] = e
        ..onClick.listen(click81)
        ..onMouseOut.listen(hoverout81)
        ..onMouseOver.listen(hoverin81);
    ++i;
    e = querySelector('#b82');
    masu_[i] = e
        ..onClick.listen(click82)
        ..onMouseOut.listen(hoverout82)
        ..onMouseOver.listen(hoverin82);
    ++i;
    e = querySelector('#b83');
    masu_[i] = e
        ..onClick.listen(click83)
        ..onMouseOut.listen(hoverout83)
        ..onMouseOver.listen(hoverin83);
    ++i;
    e = querySelector('#b84');
    masu_[i] = e
        ..onClick.listen(click84)
        ..onMouseOut.listen(hoverout84)
        ..onMouseOver.listen(hoverin84);
    ++i;
    e = querySelector('#b85');
    masu_[i] = e
        ..onClick.listen(click85)
        ..onMouseOut.listen(hoverout85)
        ..onMouseOver.listen(hoverin85);
    ++i;
    e = querySelector('#b86');
    masu_[i] = e
        ..onClick.listen(click86)
        ..onMouseOut.listen(hoverout86)
        ..onMouseOver.listen(hoverin86);
    ++i;
    e = querySelector('#b87');
    masu_[i] = e
        ..onClick.listen(click87)
        ..onMouseOut.listen(hoverout87)
        ..onMouseOver.listen(hoverin87);
    ++i;
    e = querySelector('#b88');
    masu_[i] = e
        ..onClick.listen(click88)
        ..onMouseOut.listen(hoverout88)
        ..onMouseOver.listen(hoverin88);
    ++i;
    e = querySelector('#b89');
    masu_[i] = e
        ..onClick.listen(click89)
        ..onMouseOut.listen(hoverout89)
        ..onMouseOver.listen(hoverin89);
    ++i;
    e = querySelector('#b91');
    masu_[i] = e
        ..onClick.listen(click91)
        ..onMouseOut.listen(hoverout91)
        ..onMouseOver.listen(hoverin91);
    ++i;
    e = querySelector('#b92');
    masu_[i] = e
        ..onClick.listen(click92)
        ..onMouseOut.listen(hoverout92)
        ..onMouseOver.listen(hoverin92);
    ++i;
    e = querySelector('#b93');
    masu_[i] = e
        ..onClick.listen(click93)
        ..onMouseOut.listen(hoverout93)
        ..onMouseOver.listen(hoverin93);
    ++i;
    e = querySelector('#b94');
    masu_[i] = e
        ..onClick.listen(click94)
        ..onMouseOut.listen(hoverout94)
        ..onMouseOver.listen(hoverin94);
    ++i;
    e = querySelector('#b95');
    masu_[i] = e
        ..onClick.listen(click95)
        ..onMouseOut.listen(hoverout95)
        ..onMouseOver.listen(hoverin95);
    ++i;
    e = querySelector('#b96');
    masu_[i] = e
        ..onClick.listen(click96)
        ..onMouseOut.listen(hoverout96)
        ..onMouseOver.listen(hoverin96);
    ++i;
    e = querySelector('#b97');
    masu_[i] = e
        ..onClick.listen(click97)
        ..onMouseOut.listen(hoverout97)
        ..onMouseOver.listen(hoverin97);
    ++i;
    e = querySelector('#b98');
    masu_[i] = e
        ..onClick.listen(click98)
        ..onMouseOut.listen(hoverout98)
        ..onMouseOver.listen(hoverin98);
    ++i;
    e = querySelector('#b99');
    masu_[i] = e
        ..onClick.listen(click99)
        ..onMouseOut.listen(hoverout99)
        ..onMouseOver.listen(hoverin99);
  }

  // abstract click functions
  void click11(MouseEvent e) {absclick(0, 0, e);}
  void click12(MouseEvent e) {absclick(0, 1, e);}
  void click13(MouseEvent e) {absclick(0, 2, e);}
  void click14(MouseEvent e) {absclick(0, 3, e);}
  void click15(MouseEvent e) {absclick(0, 4, e);}
  void click16(MouseEvent e) {absclick(0, 5, e);}
  void click17(MouseEvent e) {absclick(0, 6, e);}
  void click18(MouseEvent e) {absclick(0, 7, e);}
  void click19(MouseEvent e) {absclick(0, 8, e);}
  void click21(MouseEvent e) {absclick(1, 0, e);}
  void click22(MouseEvent e) {absclick(1, 1, e);}
  void click23(MouseEvent e) {absclick(1, 2, e);}
  void click24(MouseEvent e) {absclick(1, 3, e);}
  void click25(MouseEvent e) {absclick(1, 4, e);}
  void click26(MouseEvent e) {absclick(1, 5, e);}
  void click27(MouseEvent e) {absclick(1, 6, e);}
  void click28(MouseEvent e) {absclick(1, 7, e);}
  void click29(MouseEvent e) {absclick(1, 8, e);}
  void click31(MouseEvent e) {absclick(2, 0, e);}
  void click32(MouseEvent e) {absclick(2, 1, e);}
  void click33(MouseEvent e) {absclick(2, 2, e);}
  void click34(MouseEvent e) {absclick(2, 3, e);}
  void click35(MouseEvent e) {absclick(2, 4, e);}
  void click36(MouseEvent e) {absclick(2, 5, e);}
  void click37(MouseEvent e) {absclick(2, 6, e);}
  void click38(MouseEvent e) {absclick(2, 7, e);}
  void click39(MouseEvent e) {absclick(2, 8, e);}
  void click41(MouseEvent e) {absclick(3, 0, e);}
  void click42(MouseEvent e) {absclick(3, 1, e);}
  void click43(MouseEvent e) {absclick(3, 2, e);}
  void click44(MouseEvent e) {absclick(3, 3, e);}
  void click45(MouseEvent e) {absclick(3, 4, e);}
  void click46(MouseEvent e) {absclick(3, 5, e);}
  void click47(MouseEvent e) {absclick(3, 6, e);}
  void click48(MouseEvent e) {absclick(3, 7, e);}
  void click49(MouseEvent e) {absclick(3, 8, e);}
  void click51(MouseEvent e) {absclick(4, 0, e);}
  void click52(MouseEvent e) {absclick(4, 1, e);}
  void click53(MouseEvent e) {absclick(4, 2, e);}
  void click54(MouseEvent e) {absclick(4, 3, e);}
  void click55(MouseEvent e) {absclick(4, 4, e);}
  void click56(MouseEvent e) {absclick(4, 5, e);}
  void click57(MouseEvent e) {absclick(4, 6, e);}
  void click58(MouseEvent e) {absclick(4, 7, e);}
  void click59(MouseEvent e) {absclick(4, 8, e);}
  void click61(MouseEvent e) {absclick(5, 0, e);}
  void click62(MouseEvent e) {absclick(5, 1, e);}
  void click63(MouseEvent e) {absclick(5, 2, e);}
  void click64(MouseEvent e) {absclick(5, 3, e);}
  void click65(MouseEvent e) {absclick(5, 4, e);}
  void click66(MouseEvent e) {absclick(5, 5, e);}
  void click67(MouseEvent e) {absclick(5, 6, e);}
  void click68(MouseEvent e) {absclick(5, 7, e);}
  void click69(MouseEvent e) {absclick(5, 8, e);}
  void click71(MouseEvent e) {absclick(6, 0, e);}
  void click72(MouseEvent e) {absclick(6, 1, e);}
  void click73(MouseEvent e) {absclick(6, 2, e);}
  void click74(MouseEvent e) {absclick(6, 3, e);}
  void click75(MouseEvent e) {absclick(6, 4, e);}
  void click76(MouseEvent e) {absclick(6, 5, e);}
  void click77(MouseEvent e) {absclick(6, 6, e);}
  void click78(MouseEvent e) {absclick(6, 7, e);}
  void click79(MouseEvent e) {absclick(6, 8, e);}
  void click81(MouseEvent e) {absclick(7, 0, e);}
  void click82(MouseEvent e) {absclick(7, 1, e);}
  void click83(MouseEvent e) {absclick(7, 2, e);}
  void click84(MouseEvent e) {absclick(7, 3, e);}
  void click85(MouseEvent e) {absclick(7, 4, e);}
  void click86(MouseEvent e) {absclick(7, 5, e);}
  void click87(MouseEvent e) {absclick(7, 6, e);}
  void click88(MouseEvent e) {absclick(7, 7, e);}
  void click89(MouseEvent e) {absclick(7, 8, e);}
  void click91(MouseEvent e) {absclick(8, 0, e);}
  void click92(MouseEvent e) {absclick(8, 1, e);}
  void click93(MouseEvent e) {absclick(8, 2, e);}
  void click94(MouseEvent e) {absclick(8, 3, e);}
  void click95(MouseEvent e) {absclick(8, 4, e);}
  void click96(MouseEvent e) {absclick(8, 5, e);}
  void click97(MouseEvent e) {absclick(8, 6, e);}
  void click98(MouseEvent e) {absclick(8, 7, e);}
  void click99(MouseEvent e) {absclick(8, 8, e);}

  // abstract hover-in functions
  void hoverin11(MouseEvent e) {abshoverin(0, 0);}
  void hoverin12(MouseEvent e) {abshoverin(0, 1);}
  void hoverin13(MouseEvent e) {abshoverin(0, 2);}
  void hoverin14(MouseEvent e) {abshoverin(0, 3);}
  void hoverin15(MouseEvent e) {abshoverin(0, 4);}
  void hoverin16(MouseEvent e) {abshoverin(0, 5);}
  void hoverin17(MouseEvent e) {abshoverin(0, 6);}
  void hoverin18(MouseEvent e) {abshoverin(0, 7);}
  void hoverin19(MouseEvent e) {abshoverin(0, 8);}
  void hoverin21(MouseEvent e) {abshoverin(1, 0);}
  void hoverin22(MouseEvent e) {abshoverin(1, 1);}
  void hoverin23(MouseEvent e) {abshoverin(1, 2);}
  void hoverin24(MouseEvent e) {abshoverin(1, 3);}
  void hoverin25(MouseEvent e) {abshoverin(1, 4);}
  void hoverin26(MouseEvent e) {abshoverin(1, 5);}
  void hoverin27(MouseEvent e) {abshoverin(1, 6);}
  void hoverin28(MouseEvent e) {abshoverin(1, 7);}
  void hoverin29(MouseEvent e) {abshoverin(1, 8);}
  void hoverin31(MouseEvent e) {abshoverin(2, 0);}
  void hoverin32(MouseEvent e) {abshoverin(2, 1);}
  void hoverin33(MouseEvent e) {abshoverin(2, 2);}
  void hoverin34(MouseEvent e) {abshoverin(2, 3);}
  void hoverin35(MouseEvent e) {abshoverin(2, 4);}
  void hoverin36(MouseEvent e) {abshoverin(2, 5);}
  void hoverin37(MouseEvent e) {abshoverin(2, 6);}
  void hoverin38(MouseEvent e) {abshoverin(2, 7);}
  void hoverin39(MouseEvent e) {abshoverin(2, 8);}
  void hoverin41(MouseEvent e) {abshoverin(3, 0);}
  void hoverin42(MouseEvent e) {abshoverin(3, 1);}
  void hoverin43(MouseEvent e) {abshoverin(3, 2);}
  void hoverin44(MouseEvent e) {abshoverin(3, 3);}
  void hoverin45(MouseEvent e) {abshoverin(3, 4);}
  void hoverin46(MouseEvent e) {abshoverin(3, 5);}
  void hoverin47(MouseEvent e) {abshoverin(3, 6);}
  void hoverin48(MouseEvent e) {abshoverin(3, 7);}
  void hoverin49(MouseEvent e) {abshoverin(3, 8);}
  void hoverin51(MouseEvent e) {abshoverin(4, 0);}
  void hoverin52(MouseEvent e) {abshoverin(4, 1);}
  void hoverin53(MouseEvent e) {abshoverin(4, 2);}
  void hoverin54(MouseEvent e) {abshoverin(4, 3);}
  void hoverin55(MouseEvent e) {abshoverin(4, 4);}
  void hoverin56(MouseEvent e) {abshoverin(4, 5);}
  void hoverin57(MouseEvent e) {abshoverin(4, 6);}
  void hoverin58(MouseEvent e) {abshoverin(4, 7);}
  void hoverin59(MouseEvent e) {abshoverin(4, 8);}
  void hoverin61(MouseEvent e) {abshoverin(5, 0);}
  void hoverin62(MouseEvent e) {abshoverin(5, 1);}
  void hoverin63(MouseEvent e) {abshoverin(5, 2);}
  void hoverin64(MouseEvent e) {abshoverin(5, 3);}
  void hoverin65(MouseEvent e) {abshoverin(5, 4);}
  void hoverin66(MouseEvent e) {abshoverin(5, 5);}
  void hoverin67(MouseEvent e) {abshoverin(5, 6);}
  void hoverin68(MouseEvent e) {abshoverin(5, 7);}
  void hoverin69(MouseEvent e) {abshoverin(5, 8);}
  void hoverin71(MouseEvent e) {abshoverin(6, 0);}
  void hoverin72(MouseEvent e) {abshoverin(6, 1);}
  void hoverin73(MouseEvent e) {abshoverin(6, 2);}
  void hoverin74(MouseEvent e) {abshoverin(6, 3);}
  void hoverin75(MouseEvent e) {abshoverin(6, 4);}
  void hoverin76(MouseEvent e) {abshoverin(6, 5);}
  void hoverin77(MouseEvent e) {abshoverin(6, 6);}
  void hoverin78(MouseEvent e) {abshoverin(6, 7);}
  void hoverin79(MouseEvent e) {abshoverin(6, 8);}
  void hoverin81(MouseEvent e) {abshoverin(7, 0);}
  void hoverin82(MouseEvent e) {abshoverin(7, 1);}
  void hoverin83(MouseEvent e) {abshoverin(7, 2);}
  void hoverin84(MouseEvent e) {abshoverin(7, 3);}
  void hoverin85(MouseEvent e) {abshoverin(7, 4);}
  void hoverin86(MouseEvent e) {abshoverin(7, 5);}
  void hoverin87(MouseEvent e) {abshoverin(7, 6);}
  void hoverin88(MouseEvent e) {abshoverin(7, 7);}
  void hoverin89(MouseEvent e) {abshoverin(7, 8);}
  void hoverin91(MouseEvent e) {abshoverin(8, 0);}
  void hoverin92(MouseEvent e) {abshoverin(8, 1);}
  void hoverin93(MouseEvent e) {abshoverin(8, 2);}
  void hoverin94(MouseEvent e) {abshoverin(8, 3);}
  void hoverin95(MouseEvent e) {abshoverin(8, 4);}
  void hoverin96(MouseEvent e) {abshoverin(8, 5);}
  void hoverin97(MouseEvent e) {abshoverin(8, 6);}
  void hoverin98(MouseEvent e) {abshoverin(8, 7);}
  void hoverin99(MouseEvent e) {abshoverin(8, 8);}

  // abstract hover-out functions
  void hoverout11(MouseEvent e) {abshoverout(0, 0);}
  void hoverout12(MouseEvent e) {abshoverout(0, 1);}
  void hoverout13(MouseEvent e) {abshoverout(0, 2);}
  void hoverout14(MouseEvent e) {abshoverout(0, 3);}
  void hoverout15(MouseEvent e) {abshoverout(0, 4);}
  void hoverout16(MouseEvent e) {abshoverout(0, 5);}
  void hoverout17(MouseEvent e) {abshoverout(0, 6);}
  void hoverout18(MouseEvent e) {abshoverout(0, 7);}
  void hoverout19(MouseEvent e) {abshoverout(0, 8);}
  void hoverout21(MouseEvent e) {abshoverout(1, 0);}
  void hoverout22(MouseEvent e) {abshoverout(1, 1);}
  void hoverout23(MouseEvent e) {abshoverout(1, 2);}
  void hoverout24(MouseEvent e) {abshoverout(1, 3);}
  void hoverout25(MouseEvent e) {abshoverout(1, 4);}
  void hoverout26(MouseEvent e) {abshoverout(1, 5);}
  void hoverout27(MouseEvent e) {abshoverout(1, 6);}
  void hoverout28(MouseEvent e) {abshoverout(1, 7);}
  void hoverout29(MouseEvent e) {abshoverout(1, 8);}
  void hoverout31(MouseEvent e) {abshoverout(2, 0);}
  void hoverout32(MouseEvent e) {abshoverout(2, 1);}
  void hoverout33(MouseEvent e) {abshoverout(2, 2);}
  void hoverout34(MouseEvent e) {abshoverout(2, 3);}
  void hoverout35(MouseEvent e) {abshoverout(2, 4);}
  void hoverout36(MouseEvent e) {abshoverout(2, 5);}
  void hoverout37(MouseEvent e) {abshoverout(2, 6);}
  void hoverout38(MouseEvent e) {abshoverout(2, 7);}
  void hoverout39(MouseEvent e) {abshoverout(2, 8);}
  void hoverout41(MouseEvent e) {abshoverout(3, 0);}
  void hoverout42(MouseEvent e) {abshoverout(3, 1);}
  void hoverout43(MouseEvent e) {abshoverout(3, 2);}
  void hoverout44(MouseEvent e) {abshoverout(3, 3);}
  void hoverout45(MouseEvent e) {abshoverout(3, 4);}
  void hoverout46(MouseEvent e) {abshoverout(3, 5);}
  void hoverout47(MouseEvent e) {abshoverout(3, 6);}
  void hoverout48(MouseEvent e) {abshoverout(3, 7);}
  void hoverout49(MouseEvent e) {abshoverout(3, 8);}
  void hoverout51(MouseEvent e) {abshoverout(4, 0);}
  void hoverout52(MouseEvent e) {abshoverout(4, 1);}
  void hoverout53(MouseEvent e) {abshoverout(4, 2);}
  void hoverout54(MouseEvent e) {abshoverout(4, 3);}
  void hoverout55(MouseEvent e) {abshoverout(4, 4);}
  void hoverout56(MouseEvent e) {abshoverout(4, 5);}
  void hoverout57(MouseEvent e) {abshoverout(4, 6);}
  void hoverout58(MouseEvent e) {abshoverout(4, 7);}
  void hoverout59(MouseEvent e) {abshoverout(4, 8);}
  void hoverout61(MouseEvent e) {abshoverout(5, 0);}
  void hoverout62(MouseEvent e) {abshoverout(5, 1);}
  void hoverout63(MouseEvent e) {abshoverout(5, 2);}
  void hoverout64(MouseEvent e) {abshoverout(5, 3);}
  void hoverout65(MouseEvent e) {abshoverout(5, 4);}
  void hoverout66(MouseEvent e) {abshoverout(5, 5);}
  void hoverout67(MouseEvent e) {abshoverout(5, 6);}
  void hoverout68(MouseEvent e) {abshoverout(5, 7);}
  void hoverout69(MouseEvent e) {abshoverout(5, 8);}
  void hoverout71(MouseEvent e) {abshoverout(6, 0);}
  void hoverout72(MouseEvent e) {abshoverout(6, 1);}
  void hoverout73(MouseEvent e) {abshoverout(6, 2);}
  void hoverout74(MouseEvent e) {abshoverout(6, 3);}
  void hoverout75(MouseEvent e) {abshoverout(6, 4);}
  void hoverout76(MouseEvent e) {abshoverout(6, 5);}
  void hoverout77(MouseEvent e) {abshoverout(6, 6);}
  void hoverout78(MouseEvent e) {abshoverout(6, 7);}
  void hoverout79(MouseEvent e) {abshoverout(6, 8);}
  void hoverout81(MouseEvent e) {abshoverout(7, 0);}
  void hoverout82(MouseEvent e) {abshoverout(7, 1);}
  void hoverout83(MouseEvent e) {abshoverout(7, 2);}
  void hoverout84(MouseEvent e) {abshoverout(7, 3);}
  void hoverout85(MouseEvent e) {abshoverout(7, 4);}
  void hoverout86(MouseEvent e) {abshoverout(7, 5);}
  void hoverout87(MouseEvent e) {abshoverout(7, 6);}
  void hoverout88(MouseEvent e) {abshoverout(7, 7);}
  void hoverout89(MouseEvent e) {abshoverout(7, 8);}
  void hoverout91(MouseEvent e) {abshoverout(8, 0);}
  void hoverout92(MouseEvent e) {abshoverout(8, 1);}
  void hoverout93(MouseEvent e) {abshoverout(8, 2);}
  void hoverout94(MouseEvent e) {abshoverout(8, 3);}
  void hoverout95(MouseEvent e) {abshoverout(8, 4);}
  void hoverout96(MouseEvent e) {abshoverout(8, 5);}
  void hoverout97(MouseEvent e) {abshoverout(8, 6);}
  void hoverout98(MouseEvent e) {abshoverout(8, 7);}
  void hoverout99(MouseEvent e) {abshoverout(8, 8);}

  /**
   * マスをクリックした。
   * 
   * @param {int} x 筋
   * @param {int} y 段
   */
  void absclick(int x, int y, MouseEvent me) {
    if (disabled)
      return;

    //Element e = at(x, y);
    sg_.onclickedMasu(x, y, me.screen);
  }
  /**
   * マスにカーソルが入った。
   * 
   * @param {int} x 筋
   * @param {int} y 段
   */
  void abshoverin(int x, int y) {
    Element e = at(x, y);
    hovercell(e);
  }
  /**
   * マスからカーソルが出た。
   * 
   * @param {int} x 筋
   * @param {int} y 段
   */
  void abshoverout(int x, int y) {
    Element e = at(x, y);
    hovercell(null);
  }

  /**
   * 指定したマスのHTML要素を返す。
   *
   * @param {int} x 筋
   * @param {int} y 段
   */
  Element at(int x, int y) => masu_[y+x*Ban.SUJI];

  /**
   * カーソルが乗っているマスを強調する。
   * masuがnullなら元に戻す。
   *
   * @param {Element} masui 強調するマス目
   */
  void hovercell(Element masui) {
    if (masui == null) {
      if (hovermasui_ != null) {
        hovermasui_.style.backgroundColor = colorBan_;
      }
    } else {
      masui.style.backgroundColor = colorHover_;
    }
    hovermasui_ = masui;
  }
  void update() {
    Ban ban = sg_.ban_;
    Element e;
    for (int i = 0 ; i < Ban.DAN ; ++i) {
      for (int j = 0 ; j < Ban.SUJI ; ++j) {
        e = at(j, i);
        e.text = ban.at(j, i).getStr();
      }
    }
  }

  /**
   * マスを選択状態表示にする。
   *
   * @param {Object} masui 対象のマス
   * @param {Boolean} b true:選択状態にする,false:選択状態を解除する
   */
  void setactivecell(Element masui, bool b) {
    if (b) {
      masui.style.border = '2px solid ' + colorActive_;
    } else {
      masui.style.border = '2px solid black';
    }
  }

  void activatemasu(int x, int y, bool bactive) {
    Element masui = at(x, y);
    setactivecell(masui, bactive);
  }

  /**
   * 移動可能なマスのハイライト制御
   *
   * @param {MasuPosList} movable 移動可能なマスのリスト
   * @param {Boolean} b true:ハイライトする, false:ハイライトしない
   */
  void activatemovable(MasuPosList movable, bool b) {
    String c;
    if (b) {
      c = colorMovable_;
    } else {
      c = colorBan_;
    }
    for (int idx = 0 ; idx < movable.size() ; ++idx) {
      int x = movable[idx].suji;
      int y = movable[idx].dan;
      Element masui = at(x, y);
      if (masui != null) {
        masui.style.backgroundColor = c;
      }
    }
  }

  bool get enabled => enabled_;
  bool get disabled => (!enabled_);
  set enabled(bool b) => enabled_ = b;
}

class TegomaUI
{
  List<Element> masukoma_;
  List<Element> masunum_;
  int teban_;
  Tegoma tgm_;
  ShogiGame sg_;
  bool enabled_;

  TegomaUI(int teban, ShogiGame sg) {
    masukoma_ = new List(Tegoma.NKOMA);
    masunum_ = new List(Tegoma.NKOMA);
    teban_ = teban;
    tgm_ = null;  // (teban == Koma.SENTEBAN) ? sg.sentegoma_ : sg.gotegoma_;
    sg_ = sg;
  }

  /**
   * 手番(teban_)を考慮した初期化
   * 
   */
  void init2() {
    String idstr;
    Element e;
    int i = 0;

    if (teban_ == Koma.SENTEBAN) {
      idstr = '#sg_';
    } else {
      idstr = '#gg_';
    }

    // 先手
    // 歩
    e = querySelector(idstr+'fu_img');
    masukoma_[i] = e
        ..onClick.listen(clicktgfu);
        //..onMouseOut.listen(hoveroutstfu)
        //..onMouseOver.listen(hoverinfu);
    e = querySelector(idstr+'fu_num');
    masunum_[i] = e
        ..onClick.listen(clicktgfu);
        //..onMouseOut.listen(hoveroutstfu)
        //..onMouseOver.listen(hoverinfu);

    // 香車
    ++i;
    e = querySelector(idstr+'kyo_img');
    masukoma_[i] = e
        ..onClick.listen(clicktgky);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'kyo_num');
    masunum_[i] = e
        ..onClick.listen(clicktgky);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    //桂馬
    ++i;
    e = querySelector(idstr+'kei_img');
    masukoma_[i] = e
        ..onClick.listen(clicktgke);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'kei_num');
    masunum_[i] = e
        ..onClick.listen(clicktgke);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    //銀将
    ++i;
    e = querySelector(idstr+'gin_img');
    masukoma_[i] = e
        ..onClick.listen(clicktggi);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'gin_num');
    masunum_[i] = e
        ..onClick.listen(clicktggi);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    //金将
    ++i;
    e = querySelector(idstr+'kin_img');
    masukoma_[i] = e
        ..onClick.listen(clicktgki);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'kin_num');
    masunum_[i] = e
        ..onClick.listen(clicktgki);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    //角行
    ++i;
    e = querySelector(idstr+'kaku_img');
    masukoma_[i] = e
        ..onClick.listen(clicktgka);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'kaku_num');
    masunum_[i] = e
        ..onClick.listen(clicktgka);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    //飛車
    ++i;
    e = querySelector(idstr+'hisha_img');
    masukoma_[i] = e
        ..onClick.listen(clicktghi);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
    e = querySelector(idstr+'hisha_num');
    masunum_[i] = e
        ..onClick.listen(clicktghi);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);

    enabled_ = false;
  }

  void clicktgfu(MouseEvent e) {absclicktg(Koma.FuID);}
  void clicktgky(MouseEvent e) {absclicktg(Koma.KyoshaID);}
  void clicktgke(MouseEvent e) {absclicktg(Koma.KeimaID);}
  void clicktggi(MouseEvent e) {absclicktg(Koma.GinID);}
  void clicktgki(MouseEvent e) {absclicktg(Koma.KinID);}
  void clicktgka(MouseEvent e) {absclicktg(Koma.KakuID);}
  void clicktghi(MouseEvent e) {absclicktg(Koma.HishaID);}

  /**
   * 手駒をクリックした
   *
   * @param {int} i 駒ID
   */
  void absclicktg(int i) {
    if (disabled)
      return;

    Element ekoma = masukoma_[i];
    Element en = masunum_[i];
    sg_.onclickedTegoma(teban_, i);
  }

  void update() {
    tgm_ = (teban_ == Koma.SENTEBAN) ? sg_.sentegoma_ : sg_.gotegoma_;
    for (int id = Koma.FuID ; id < Koma.GyokuID ; ++id) {
      int n = tgm_.getNum(id);
      Element ekoma = masukoma_[id];
      Element en = masunum_[id];
      if (n == 0) {
        ekoma.text = '';
        en.text = '';
      } else if (n == 1) {
        ekoma.text = tgm_.example(id).getStr();
        en.text = '';
      } else {
        ekoma.text = tgm_.example(id).getStr();
        en.text = n.toString();
      }
    }
  }

  set enabled(bool b) => enabled_ = b;
  bool get enabled => enabled_;
  bool get disabled => !enabled_;
}

class NariMenuUI
{
  Element menu_;
  Element nari_;
  Element funari_;
  ShogiGame sg_;
  bool visible_;

  bool get visible => visible_;

  NariMenuUI(ShogiGame sg) {
    visible_ = false;
    sg_ = sg;

    menu_ = querySelector('#narimenu');

    nari_ = querySelector('#naru')
        ..onClick.listen(clicknari);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);

    funari_ = querySelector('#narazu')
        ..onClick.listen(clickfunari);
        //..onMouseOut.listen(hoveroutstky)
        //..onMouseOver.listen(hoverinky);
  }

  /**
   * 成りをクリックした。
   */
  void clicknari(MouseEvent e) {
    sg_.onclickedNari(Koma.NARI);
  }

  /**
   * 不成りをクリックした。
   */
  void clickfunari(MouseEvent e) {
    sg_.onclickedNari(Koma.NARAZU);
  }

  /**
   * 成りメニューを表示する。
   * 
   * @param x position in pixel
   * @param y position in pixel
   */
  void popup(int x, int y) {
    visible_ = true;

    int px, py;
    //Rectangle rect = menu_.getBoundingClientRect(); 
    //px = x-rect.width;//.clientWidth;
    //py = y-rect.height;//.clientHeight;
    px = x-menu_.clientWidth;
    py = y-menu_.clientHeight;
    menu_.style.display = '';
    menu_.style.left = px.toString() + 'px';
    menu_.style.top = py.toString() + 'px';
    menu_.style.visibility = 'visible';
  }

  /**
   * 成りメニューを消す。
   */
  void hide() {
    visible_ = false;
    menu_.style.visibility = 'hidden';
  }
}

class CtrlBarUI
{
  ShogiGame sg_;
  ButtonElement btntest_;
  ButtonElement btninit_;
  ButtonElement btnstart_;
  ButtonElement btnstop_;
  ButtonElement btngiveup_;

  CtrlBarUI(ShogiGame sg) {
    sg_ = sg;
    btntest_ = querySelector('#btntest')
        ..onClick.listen(clicktest);
    btninit_ = querySelector('#btninit')
        ..onClick.listen(clickinit);
    btnstart_ = querySelector('#btnstart')
        ..onClick.listen(clickstart);
    btnstop_ = querySelector('#btnstop')
        ..onClick.listen(clickstop);
    btngiveup_ = querySelector('#btngiveup')
        ..onClick.listen(clickgiveup);
  }

  void clicktest(MouseEvent e) {
    sg_.ontest();
  }
  void clickinit(MouseEvent e) {
    sg_.oninit();
  }
  void clickstart(MouseEvent e) {
    btninit_.disabled = true;
    btnstart_.disabled = true;
    btnstop_.disabled = false;
    sg_.onstart();
  }
  void clickstop(MouseEvent e) {
    btninit_.disabled = false;
    btnstart_.disabled = false;
    btnstop_.disabled = true;
    sg_.onstop();
  }
  void clickgiveup(MouseEvent e) {
    sg_.ongiveup();
  }
}

class ShogiGame
{
  static int TAIKYOKU_READY = 0;
  static int TAIKYOKU_CHU = 1;
  static int TAIKYOKU_CHUDAN = 2;

  Ban ban_;
  Tegoma sentegoma_;
  Tegoma gotegoma_;

  Koma activekoma_;
  int ox_, oy_;
  int nx_, ny_;

  BanUI banui_;
  TegomaUI sentegomaui_;
  TegomaUI gotegomaui_;
  NariMenuUI menu_;
  CtrlBarUI barui_;

  ShogiGame() {
    banui_ = new BanUI(this);
    banui_.init();
    sentegomaui_ = new TegomaUI(Koma.SENTEBAN, this);
    sentegomaui_.init2();
    gotegomaui_ = new TegomaUI(Koma.GOTEBAN, this);
    gotegomaui_.init2();
    menu_ = new NariMenuUI(this);
    barui_ = new CtrlBarUI(this);
  }

  int teban_;
  int taikyokuchu_;
  MasuPosList activemovable_;

  void switch_teban() {
    teban_ = (teban_ == Koma.SENTEBAN) ? Koma.GOTEBAN : Koma.SENTEBAN;
  }
  void onclickedTegoma(int teban, int id) {
    // 成りメニューが表示されている間は反応しない。
    if (menu_.visible)
      return;
  }

  void ontest() {
  }

  void oninit() {
    ban_ = new Ban();
    banui_.update();
    sentegoma_ = new Tegoma(Koma.SENTEBAN);
    sentegomaui_.update();
    gotegoma_ = new Tegoma(Koma.SENTEBAN);
    gotegomaui_.update();

    taikyokuchu_ = TAIKYOKU_READY;
    activekoma_ = null;
  }

  void onstart() {
    teban_ = Koma.SENTEBAN;
    taikyokuchu_ = TAIKYOKU_CHU;
    banui_.enabled = true;
    //activeteban = Koma.SENTEBAN;
    //mykifu.putHeader(nameSente.value, nameGote.value);
    //update_screen();
  }

  void onstop() {
    taikyokuchu_ = TAIKYOKU_CHUDAN;
  }

  void ongiveup() {
  }

  /**
   * 盤上のマスをクリックした
   * 
   * @param {int} x 筋
   * @param {int} y 段
   */
  void onclickedMasu(int x, int y, Point mouse) {
    // 対局中でなければ反応しない
    if (taikyokuchu_ != TAIKYOKU_CHU)
      return;

    // 成りメニューが表示されている間は反応しない。
    if (menu_.visible)
      return;

    Koma k = ban_.at(x, y);

    if (activekoma_ == null) {
      // 移動元を指定

      // 手番じゃない駒をクリックしたら抜ける
      if (k.teban != teban_)
        return;

      ox_ = x;
      oy_ = y;
      activekoma_ = k;

      banui_.activatemasu(x, y, true);

      // 動けるマスを計算
      activemovable_ = ban_.getMovable(x, y);
      // 動けるマスをハイライト
      banui_.activatemovable(activemovable_, true);
    } else {
      if (k.teban == teban_) {
        // 選択を解除する
        activekoma_ = null;
        banui_.activatemasu(ox_, oy_, false);
        // 動けるマスのハイライトをやめる
        banui_.activatemovable(activemovable_, false);
      } else if (k.teban == Koma.AKI) {
        // 移動可能な場所であれば
        bool bmovable = false;
        for (int i = 0 ; i < activemovable_.size(); ++i) {
          MasuPos mp = activemovable_.at(i);
          if (mp.suji == x && mp.dan == y) {
            bmovable = true;
            break;
          }
        }
        if (bmovable == false) {  // 移動できないマス
          // 選択を解除する
          activekoma_ = null;
          banui_.activatemasu(ox_, oy_, false);
          // 動けるマスのハイライトをやめる
          banui_.activatemovable(activemovable_, false);
        } else {
          // 成れるかどうかを確認
          int nareru = activekoma_.checkNari(oy_, y);
          if (nareru == Koma.NARERU) {
            nx_ = x;
            ny_ = y;
            menu_.popup(mouse.x, mouse.y);
          } else {
            // 移動する
            ban_.move(ox_, oy_, x, y, nareru);

            // アクティブ表示を止める
            activekoma_ = null;
            banui_.activatemasu(ox_, oy_, false);
            // 動けるマスのハイライトをやめる
            banui_.activatemovable(activemovable_, false);
            banui_.update();
            switch_teban();
          }
        }
      } else {
        // 移動可能な場所であれば
        bool bmovable = false;
        for (int i = 0 ; i < activemovable_.size(); ++i) {
          MasuPos mp = activemovable_.at(i);
          if (mp.suji == x && mp.dan == y) {
            bmovable = true;
            break;
          }
        }
        if (bmovable == false) {  // 移動できないマス
          // 選択を解除する
          activekoma_ = null;
          banui_.activatemasu(ox_, oy_, false);
          // 動けるマスのハイライトをやめる
          banui_.activatemovable(activemovable_, false);
        } else {
          // 駒を取って

          // 成れるかどうかを確認
          int nareru = activekoma_.checkNari(oy_, y);
          if (nareru == Koma.NARERU) {
            nx_ = x;
            ny_ = y;
            menu_.popup(mouse.x, mouse.y);
          } else {
            // 移動する
          }
        }
      }
    }
  }

  /**
   * 成りメニューの項目をクリックした
   * 
   * @param {int} nari Koma.NARI or Koma.NARAZU
   */
  void onclickedNari(int nari) {
    // 表示されていないのにこの関数が呼ばれるのはおかしい。
    assert (menu_.visible);

    // 移動する
    ban_.move(ox_, oy_, nx_, ny_, nari);
  }
}

void main() {
  ShogiGame sg = new ShogiGame();
}
