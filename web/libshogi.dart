library libshogi;

//import 'libkoma.dart';
part 'libkoma.dart';

/**
 * マス目の情報
 */
class Masu {
  int x_;
  int y_;
  var koma_;

  Masu(int x, int y)
      :x_ = x, y_ = y, koma_ = null;

  Koma get koma => koma_;
  void setkoma(Koma k) {
    koma_ = k;
  }

  int get suji => x_;
  int get dan => y_;
}

/**
 * 持ち駒管理クラス
 */
class Tegoma {
  static int NKOMA = 7;
  int teban_;
  List<Masu> masutbl_;  // 駒のテーブル
  List<List<Koma>> mykoma_;

  Tegoma(int teban) {
    teban_ = teban;

    masutbl_ = new List(NKOMA);
    for (int i = 0; i < NKOMA ; ++i) {
      masutbl_[i] = new Masu(-1, -1);
    }
    masutbl_[Koma.FuID].setkoma(new Fu(-1, -1, teban));
    masutbl_[Koma.KyoshaID].setkoma(new Kyosha(-1, -1, teban));
    masutbl_[Koma.KeimaID].setkoma(new Keima(-1, -1, teban));
    masutbl_[Koma.GinID].setkoma(new Gin(-1, -1, teban));
    masutbl_[Koma.KinID].setkoma(new Kin(-1, -1, teban));
    masutbl_[Koma.KakuID].setkoma(new Kaku(-1, -1, teban));
    masutbl_[Koma.HishaID].setkoma(new Hisha(-1, -1, teban));

    mykoma_ = new List<List<Koma>>(NKOMA);
    for (int i = 0; i < NKOMA ; ++i) {
      mykoma_[i] = new List<Koma>();
    }
  }

  /**
   * 指定したIDの駒を何個持っているか
   * 
   * @param id 駒のID
   */
  int getNum(int id) {
    return mykoma_[id].length;
  }
  /**
   * 取った駒を登録する
   */
  void storeKoma(Koma k) {
    // 自分の手番をセット
    k.setTeban(teban_);
    k.setx(-1);
    k.sety(-1);

    // 格納
    mykoma_[k.id].add(k);
  }
  /**
   * 駒を取り出す
   */
  Koma takeKoma(int id) {
    if (mykoma_[id].length == 0) {
      return null;
    }
    Koma k = mykoma_[id][0];
    mykoma_[id].removeAt(0);
    return k;
  }
  Koma example(int id) {
    return masutbl_[id].koma;
  }
}

/**
 * 将棋盤クラス
 * 
 * @note １筋から９筋は０～８として扱います。
 * @note １段目から９段目は０～８として扱います。
 * @note －１段目は駒台
 */
class Ban {
  static int SUJI = 9;
  static int DAN = 9;
  List<Masu> ban_;  // [SUJI*DAN];
  Koma sentegyoku_;
  Koma gotegyoku_;
  Koma aki_;
  Tegoma sentegoma_;
  Tegoma gotegoma_;

  Ban() {
    ban_ = new List<Masu>();
    alignKoma();
  }

  void alignKoma() {
    aki_ = new Koma();
    for (int j = 0; j < DAN; ++j) {
      for (int i = 0; i < SUJI; ++i) {
        Masu masu = new Masu(i, j);
        masu.setkoma(aki_);
        ban_.add(masu);
      }
    }
    for (int i = 0; i < 9; ++i) {
      initkoma_(new Fu(Koma.GOTEBAN, i, 2));
      initkoma_(new Fu(Koma.SENTEBAN, i, 6));
    }
    initkoma_(new Kaku(Koma.GOTEBAN, 1, 1));
    initkoma_(new Kaku(Koma.SENTEBAN, 7, 7));

    initkoma_(new Hisha(Koma.GOTEBAN, 7, 1));
    initkoma_(new Hisha(Koma.SENTEBAN, 1, 7));

    gotegyoku_ = new Gyoku(Koma.GOTEBAN, 4, 0);
    sentegyoku_ = new Gyoku(Koma.SENTEBAN, 4, 8);
    initkoma_(gotegyoku_);
    initkoma_(sentegyoku_);

    initkoma_(new Kin(Koma.GOTEBAN, 3, 0));
    initkoma_(new Kin(Koma.GOTEBAN, 5, 0));
    initkoma_(new Kin(Koma.SENTEBAN, 3, 8));
    initkoma_(new Kin(Koma.SENTEBAN, 5, 8));

    initkoma_(new Gin(Koma.GOTEBAN, 2, 0));
    initkoma_(new Gin(Koma.GOTEBAN, 6, 0));
    initkoma_(new Gin(Koma.SENTEBAN, 2, 8));
    initkoma_(new Gin(Koma.SENTEBAN, 6, 8));

    initkoma_(new Keima(Koma.GOTEBAN, 1, 0));
    initkoma_(new Keima(Koma.GOTEBAN, 7, 0));
    initkoma_(new Keima(Koma.SENTEBAN, 1, 8));
    initkoma_(new Keima(Koma.SENTEBAN, 7, 8));

    initkoma_(new Kyosha(Koma.GOTEBAN, 0, 0));
    initkoma_(new Kyosha(Koma.GOTEBAN, 8, 0));
    initkoma_(new Kyosha(Koma.SENTEBAN, 0, 8));
    initkoma_(new Kyosha(Koma.SENTEBAN, 8, 8));

    sentegoma_ = new Tegoma(Koma.SENTEBAN);
    gotegoma_ = new Tegoma(Koma.GOTEBAN);
  }

  Masu masu(int x, int y) {
    return ban_[y+x*SUJI];
  }
  Koma at(int x, int y) {
    return masu(x, y).koma;
  }
  /*void swap(int x1, int y1, int x2, int y2) {
    Koma tmp = at(x2, x2);
    ban_[x2+y2*SUJI].setkoma(at(x1, y1));
    ban_[x1+y1*SUJI].setkoma(tmp);
  }*/
  void initkoma_(Koma k) {
    masu(k.x, k.y).setkoma(k);
  }

  /**
   * (x, y)の駒が動けるマスのリストを返す。
   * 
   * @return {MasuPosList} (x, y)の駒が動けるマスのリスト
   */
  MasuPosList getMovable(int x, int y) {
    MasuPosList mpl = new MasuPosList();
    Koma k = at(x, y);
    List<Movement> mvmnts = k.movement();
    for (Movement mv in mvmnts) {
      int dx = mv.dx;
      int dy = mv.dy;
      if (mv.straight) {
        // 直線的に移動
        int ax = x;
        int ay = y;
        if (k.teban == Koma.SENTEBAN) {
         dy = -dy;
        } else {
        }
        while (true) {
          ax += dx;
          ay += dy;
          if (ax < 0 || ax > 8) {
            break;
          }
          if (ay < 0 || ay > 8) {
            break;
          }
          Koma k2 = at(ax, ay);
          if (k2.teban == k.teban) {
            break;
          }
          mpl.add(new MasuPos(ax, ay));
          if (k2.teban != Koma.AKI) {
            break;
          }
        }
      } else {
        // 1マス移動
        int ax = x + dx;
        if (ax < 0 || ax > 8) {
          continue;
        }

        int ay;
        if (k.teban == Koma.SENTEBAN) {
          ay = y - dy;
        } else {
          ay = y + dy;
        }
        if (ay < 0 || ay > 8) {
          continue;
        }
        Koma k2 = at(ax, ay);
        if (k2.teban != k.teban) {
          mpl.add(new MasuPos(ax, ay));
        }
      }
    }
    return mpl;
  }
  /**
   * 打てるマスのリストを返す。
   *
   * @return {Array} 打てるマスのリスト
   */
  MasuPosList getUchable(Koma k) {
    MasuPosList mpl;
    int starty = 0;
    int endy = DAN;
    if (k.teban == Koma.SENTEBAN) {
      starty = k.uchi_limit;
    } else {
      endy = 8-k.uchi_limit;
   }
   var list = [];
   for (var i = 0; i < 9; ++i) {
      for (var j = starty; j < endy; ++j) {
        if (at(i, j).teban == Koma.AKI) {
          mpl.add(new MasuPos(i, j));
        }
      }
    }
    return mpl;
  }

  /**
   * コマを動かす。
   *
   * @param {int} fromx 移動元
   * @param {int} fromy 移動元
   * @param {int} tox 移動先
   * @param {int} toy 移動先
   * @param {int} nari 成る(Koma.NARI)か成らない(Koma.Narazu)か
   *                      成る場合は駒を裏返す(=成った駒を元に戻せる)
   *
   * @return {int} 0:正常終了, 1:動かす駒がない, 2:動かす先に自分の駒がある
   */
  int move(int fromx, int fromy, int tox, int toy, int nari) {
    Masu fromm = masu(fromx, fromy);
    Koma fromk = fromm.koma;
    Masu tom = masu(tox, toy);
    Koma tok = tom.koma;

    // 動かす駒がない
    if (fromk.teban == Koma.AKI) {
      return 1;
    }
    // 動かす先に自分の駒がある
    if (fromk.teban == tok.teban) {
      return 2;
    }

    if (nari == Koma.NARI) {
      if (fromk.nari == Koma.NARI) {
        fromk.nari = Koma.NARAZU;
      } else {
        fromk.nari = Koma.NARI;
      }
    }

    fromk.setxy(tox, toy);
    tom.setkoma(fromk);
    fromm.setkoma(aki_);


    if (tok.teban != Koma.AKI) {
      tok.setTeban(fromk.teban);
      if (fromk.teban == Koma.SENTEBAN) {
        sentegoma_.storeKoma(tok);
      } else {
        gotegoma_.storeKoma(tok);
      }
    }
    return 0;
  }
  /**
   * 駒を打つ
   *
   */
  int utsu(int teban, int id, int tox, int toy) {
    Tegoma teg;
    if (teban == Koma.SENTEBAN) {
      teg = sentegoma_;
    } else {
      teg = gotegoma_;
    }
    Koma k = teg.takeKoma(id);
    if (k == null) {
      // error
      return 1;
    }
    if (teban != k.teban) {
      return 2;
    }

    k.setxy(tox, toy);

    Masu tom = masu(tox, toy);
    tom.setkoma(k);

    return 0;
  }

  /**
   * 局面の出力(CSA)
   */
  String KyokumenCSA(int teban) {
    String kyokumen = '';
    for (int i = 0; i < DAN; ++i) {
      kyokumen += 'P' + (i + 1).toString();
      for (var j = SUJI-1; j >= 0; --j) {
        Koma koma = masu(i, j).koma;
        kyokumen += koma.getShortStrCSA();
      }
      kyokumen += '\n';
    }
    for (int i = 0 ; i < Tegoma.NKOMA ; ++i) {
      int n = sentegoma_.getNum(i);
      Koma k = sentegoma_.example(i);
      for (;n > 0;) {
        --n;
        kyokumen += 'P' + k.kifuShortCSA(-1, -1) + '\n';
      }
    }
    for (int i = 0 ; i < Tegoma.NKOMA ; ++i) {
      int n = gotegoma_.getNum(i);
      Koma k = gotegoma_.example(i);
      for (;n > 0;) {
        --n;
        kyokumen += 'P' + k.kifuShortCSA(-1, -1) + '\n';
      }
    }
    //kyokumen += '\nP-00AL\n';  // 残りは全部後手の駒台の上
    if (teban == Koma.SENTEBAN) {
     kyokumen += '+';
    } else {
     kyokumen += '-';
    }
    return kyokumen;
  }

  /**
   * 局面の出力KIF
   *
   * @return {String} 局面のデータ文字列
   */
  String KyokumenKIF(int nteme, String lastte) {
    Koma k;
    String kyokumen = '後手の持駒：';
    String komadai = '';
    Tegoma tgm = gotegoma_;
    for (int i = 0 ; i < Tegoma.NKOMA ; ++i) {
      int n = tgm.getNum(i);
      if (n != 0) {
        k = tgm.example(i);
        komadai += k.strtypeKIF;
        komadai += Koma.KanjiNum[n];
        komadai += '　';
      }
    }
    if (komadai == '') {
      komadai = 'なし';
    }
    kyokumen += komadai;
    kyokumen += '\n  ９ ８ ７ ６ ５ ４ ３ ２ １\n+---------------------------+\n';
    for (int i = 0; i < 9; ++i) {
      kyokumen += '|';
      for (int j = 8; j >= 0; --j) {
        k = at(j, i);
        kyokumen += k.getShortStrKIF();
      }
      kyokumen += '|' + Koma.KanjiNum[i] + '\n';
    }
    kyokumen += '+---------------------------+\n先手の持駒：';

    komadai = '';
    tgm = sentegoma_;
    for (int i = 0 ; i < Tegoma.NKOMA ; ++i) {
      int n = tgm.getNum(i);
      if (n != 0) {
        k = tgm.example(i);
        komadai += k.strtypeKIF;
        komadai += Koma.KanjiNum[n];
        komadai += '　';
      }
    }
    if (komadai == '') {
      komadai = 'なし';
    }
    kyokumen += komadai;

    // 手数＝123 ５五角打まで
    kyokumen += '\n手数＝';
    kyokumen += nteme.toString() + ' ' + lastte + 'まで\n';

    return kyokumen;
  }
}
